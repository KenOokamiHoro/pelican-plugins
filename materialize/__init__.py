'''
materialize
===================================

Pelican + Materialize =w=

'''

from pelican import signals, contents
from bs4 import BeautifulSoup


def replace(searchterm, soup, attributes):
    for item in soup.findAll(searchterm):
        item.attrs['class'] = list(
            set(item.attrs.get('class', []) + attributes))


def if_replace(searchterm, soup, condiction, attributes):
    for item in soup.findAll(searchterm):
        if condiction(item):
            item.attrs['class'] = list(
                set(item.attrs.get('class', []) + attributes))


def replace_tables(soup, attributes=['highlight', 'stripped']):
    replace('table', soup, attributes)


def is_not_avatar(item):
    return not "media-object" in item.attrs.get("class", {})


def replace_images(soup, attributes=['materialboxed', 'responsive-img']):
    if_replace('img', soup, is_not_avatar, attributes)


def replace_embed(soup, attributes=['responsive-video']):
    replace('embed', soup, attributes)
    replace('iframe', soup, attributes)
    replace('video', soup, attributes)
    replace('object', soup, attributes)


def replace_table(soup, attributes=['white', 'black-text']):
    replace('table', soup, attributes)


def bootstrapify(content):
    if isinstance(content, contents.Static):
        return

    soup = BeautifulSoup(content._content, 'html.parser')
    replace_tables(soup)
    replace_images(soup)
    replace_embed(soup)
    replace_table(soup)

    content._content = soup.decode()


def register():
    signals.content_object_init.connect(bootstrapify)
